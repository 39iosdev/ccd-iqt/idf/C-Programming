
# Performance Labs 

## Write a program to implement a queue using an array

## Write a program that implements a Queue as shown below:
```
--Enqueue elements--
Enter a number to enqueue:1
1
Enter a number to enqueue:2
2 1
Enter a number to enqueue:3
3 2 1
Enter a number to enqueue:4
4 3 2 1
Enter a number to enqueue:5
5 4 3 2 1
Queue is full
 
--Dequeue elements --
dequeue element 1
5 4 3 2
dequeue element 2
5 4 3
dequeue element 3
5 4
dequeue element 4
5
dequeue element 5
 
Queue is empty
```
## Create the functions for the Linked-list Queue
```c
#include<stdio.h>   
#include<stdlib.h>  
struct node   
{  
    int data;   
    struct node *next;  
};  
struct node *front;  
struct node *rear;   
void insert();  
void delete();  
void display();  
void main ()  
{  
    int choice;   
    while(choice != 4)   
    {     
        printf("\n*************************Main Menu*****************************\n");  
        printf("\n=================================================================\n");  
        printf("\n1.insert an element\n2.Delete an element\n3.Display the queue\n4.Exit\n");  
        printf("\nEnter your choice ?");  
        scanf("%d",& choice);  
        switch(choice)  
        {  
            case 1:  
            insert();  
            break;  
            case 2:  
            delete();  
            break;  
            case 3:  
            display();  
            break;  
            case 4:  
            exit(0);  
            break;  
            default:   
            printf("\nEnter valid choice??\n");  
        }  
    }  
}  
void insert()  
{  
   ## your code here
}     
void delete ()  
{  
    ## your code here 
}  
void display()  
{  
 ## your code here 
}  
```
