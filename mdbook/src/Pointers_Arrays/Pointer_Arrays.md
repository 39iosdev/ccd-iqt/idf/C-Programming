# Pointer Arrays

* **Pointers can be stored in variables**

```
char userString1[] = { "Test?"};
```

* **Variables can be stored in arrays**

```
float x = 1.2; float y = 3.4;       // Ignore this faux pas
float cartesianCoordinate1[2] = { x, y};  // Array of floats
```
* **This means arrays can also store pointer variables**

```
char rule0[] = {"Talk about Byte Club."};
char rule1[] = {"Please talk about Byte Club."};
char rule2[] = {"If a byte is 0x0 the string is over."};
char * byteClubRules[] = {rule0, rule1, rule2};
/* byteClubRules is an array of character pointers */ for (i = 0; i < 3; i++)
}
    printf("Rule #%d:\t%s\n" , i, byteClubRules[i]);
}
```

---

* **Pointer arrays can store pointers of any legal data type**
```
float stu1Grades[5] = { 90.1, 100.0, 89.9, 78.1, 87.6};
float stu2Grades[5] = { 70.5, 79.3, 68.9, 3.0, 0.0};
float stu3Grades[5] = {100.0, 100.0, 100.0, 99.9];
float * classGrades[] = { stu1Grades, stu2Grades, stu3Grades};
/* classGrades is an array of floating point integer pointers */

float tempAvgGrade = 0;
printf("\nStudent Averages\n-------------------\n");
for (i = 0; i < 3; i++)
{
    tempAvgGrade = average_students_grades(*(classGrades + i), 5);
    printf("Student #%d:\t%2.1f", (i + 1), tempAvgGrade);
}
printf("----------------\n");
```

---
* ** Memory storage of arrays is still contiguous, regardless of data type(e.g., char, char *)**
* ** The following code represents...**
     * **...strings containing initials and ...**
     * **...an array of those strings**
```
/* Example code will be executed per line and displayed in memory */
int i = 0;
char initials0[] = { 'j', 'e', 'h', 0x0 };
char initials1[] = { 'm', 'a', 't', 0x0 };
char initials2[] = { 'c', 's', 'n', 0x0 };
char * arrayOfInits[] = { initials0, initials1, initials2};
for (i = 0; i < 3; i++)
{
    puts(*(arrayOfInits + i));
}

```

---

```
/* Example code will be executed per line and displayed in memory */
```
![](assets/Ap1.png)

---
```
int i = 0;
```

![](assets/Ap2.png)

---
```
char initials0[] = { 'j', 'e', 'h', 0x0 };
```

![](assets/Ap3.png)

---
```
char initials[] = { 'm', 'a', 't', 0x0 };
```

![](assets/Ap4.png)

---
```
char initials2[] = { 'c', 's', 'n', 0x0 };
```

![](assets/Ap5.png)

---
```
char * arrayOfInits[] = { initials0, initials1, initials2 };
```

![](assets/Ap6.png)

---
```
for (i = 0; i < 3; i++)
```

![](assets/Ap7.png)

---
```
puts(*(arrayOfInits + i));
```

![](assets/Ap8.png)

---
```
puts(*(arrayOfInits + i));
```

![](assets/Ap9.png)

---
```
for (i = 0; i< 3; i++)
```

![](assets/Ap10.png)

---
```
puts(*(arrayOfInits + i))
```

![](assets/Ap11.png)

---

---
**Lets review what happened with the below code in relation to memory.  Go through the code, step by step.


* ** Memory storage of arrays is still contiguous, regardless of data type(e.g., char, char *)**
* ** The following code represents...**
     * **...strings containing initials and ...**
     * **...an array of those strings**
```
/* Example code will be executed per line and displayed in memory */
int i = 0;
char initials0[] = { 'j', 'e', 'h', 0x0 };
char initials1[] = { 'm', 'a', 't', 0x0 };
char initials2[] = { 'c', 's', 'n', 0x0 };
char * arrayOfInits[] = { initials0, initials1, initials2};
for (i = 0; i < 3; i++)
{
    puts(*(arrayOfInits + i));
}

```
---

**In summery, "Why utilize pointer arrays?"**

* **Levels of indirections facilitate efficiency**
* **Pointers allow you to organize data without changing it**
* **An array of pointers allows you to associate data of different lengths**
----