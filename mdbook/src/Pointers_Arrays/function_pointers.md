# Function Pointers

* It is possible to define pointer variables to functions
* These function pointer variables can be 
  * placed in arrays
  * passed to other functions
  * returned by functions
* Much like other pointer variables, function pointer variables increase the modularity and flexibility of C

---
* **Use Cases**
  * Function callbacks
    * Function pointers can be passed as parameters during function calls
    * The called function will “callback” to the passed function pointer
    * Allows for modularity in chosen algorithms
    * A classic example is sorting 
  * Reference functions in a DLL
  * Object-Oriented C
    * (e.g., Linux device driver implementation)

---
* Declare a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```
  * Example 1
    * Declare a function that adds two integers and returns the sum
    * Decalare a function pointer to a function that:
      * takes two integers a parameters
      * returns and int
```
int add(int num1, int num2);         // Original function prototype
int (* addFun_ptr) (int x, int y);   // Function pointer declaration
```

---

* Declare a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```
* Example 2
  * Declare a function that splits a string at position “n” and returns the pointer to the second string
  * Declare a function pointer to a function that: A. takes a char pointer and integer as parameters and B. returns char pointer
```
char * split(char * str, int n);      // Original function prototype
char * (* splitFun_ptr) (char * word, int x);   // Function pointer var
```

---

* Declare a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```
* Example 3
  * Declare a function that swaps the contents of two integer pointers and returns an integer
  * Declare a function pointer to a function that: A. takes two integer pointers as parameters and B. returns an integer
```
int swap(int * x, int * y);	// Original function prototype
int (* swapFun_ptr)(int * a, int * b); // Function pointer var
```

---

* **Define** a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```

* Example 1
  * Declare a function that adds two integers and returns the sum
  * Declare a function pointer to a function that: A. takes two integers as parameters and B. returns an int
```
int add(int num1, int num2); 		// Original function prototype
int (* addFun_ptr)(int x, int y);	// Function pointer declaration
addFun_ptr = &add;			// Explicit assignment
```

---

* Define a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```

* Example 2
  * Declare a function that splits a string at position “n” and returns the pointer to the second string
  * Declare a function pointer to a function that: A. takes a char pointer and integer as parameters and B. returns char pointer
```
char * split(char * str, int n);	// Original function prototype
char * (* splitFun_ptr)(char * word, int x); // Function pointer var
splitFun_ptr = &split;			// Explicit assignment

```

---

* Define a function pointer
```
<return type> (* <variable name>) (<parameters list)>;
```

* Example 3
  * Declare a function that swaps the contents of two integer pointers and returns an integer
  * Declare a function pointer to a function that: A. takes two integer pointers as parameters and B. returns an integer
```
int swap(int * x, int * y);	// Original function prototype
int (* swapFun_ptr)(int * a, int * b); // Function pointer var
swapFun_ptr = swap;		// Implicit assignment
```

---
# Demonstration Lab
## Command Line Calculator
```
double add(double firstNumber, double secondNumber);
double subtract(double firstNumber, double secondNumber);
double multiply(double firstNumber, double secondNumber);
double divide(double firstNumber, double secondNumber);
```
* Define the function parameters above
* Use formatted input (e.g., scanf) to take input which will obviously invoke one of the functions above                                           (e.g., 5.1 + 7.2, -1.9 - 1.3, -13.23 * 13.37, 1 / 2.3)
* Use a conditional statement to choose the right function based on the mathematical operator and assign it to a function pointer
* Use that function pointer perform the calculation
* Print the results in a human readable format
* Beware NULL pointers and “divide by 0” errors

### Complete Performance Lab
---