# Struct Demonstration Lab
## String Splitter

Define a function that inputs a string with a delimiter and returns a student struct.

```struct student split_the_string(char * string_ptr, char delimiter);```


- Test Input Should Include:
    - Input String 
    - Delimiter
- Return a struct that contains the data from the string_ptr
- Print the human-readable results

## Sample Input

`("Charlie Chaplin, 87, 50", ',')`

## Sample Output

```
Name: Charlie Chaplin
Grade: 87
Favorateness Ranking: 50
```

The function also returns a student struct with the above data.