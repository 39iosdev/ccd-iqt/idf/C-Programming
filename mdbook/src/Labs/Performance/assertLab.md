# Assert Lab
## Walk the String

Objectives:

1. Read a string from stdin and write it to a zeroized char array.
1. Use `assert()` to verify that the last element of the char array is null.
1. Step #1- Safely write to the array.
1. Step #2- Break `assert()` by unsafely reading input into the char array.

