# Mighty Morphin'

Save the [lyrics](https://genius.com/Ron-wasserman-go-go-power-rangers-lyrics) for the Mighty Morphin' Power Rangers theme song into a text file.

Write a C program to:

- Open the file in read-only mode
- Read it char-by-char
- Print each char as it is read
- Close the file at the end
