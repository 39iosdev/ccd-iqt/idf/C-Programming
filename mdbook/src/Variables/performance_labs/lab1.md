# Lab 1: Declaring Variables

![](../../assets/engineer-humor-computer-jokes.jpg)

In this lab, you will declare and initialize variables of the following types:

* int
* float
* double
* char

Then print the variables and their sizes, using the previous lab as a reference.  We will discuss printing in later topics.

---