# Instructor Led Programming Exercises:

1. Write a program that calculates how many digits a number contains:

```sh
Enter a number: 374
The number 374 has 3 digits
```
You may assume that the number has no more than four digits. *Hint:* Use ```if``` statements to test the number. For example, if the number is between 0 and 9, it has one digit. If the number is between 10 and 99, it has two digits.

2. Write a program that asks the user for a 24-hour time, then displays the time in 12-hour form:

```sh
Enter a 24-hour time: 21:11
Equivalent 12-hour time: 9:11 PM
```

3. Modify the broker.c program of Section 5.2 by making both of the following changes:

    (a) Ask the user to enter the number of shares and the price per share, instead of the value of the trade.

    (b) Add statements that compute the commission charged by a rival broker ($33 plus 3¢ per share for fewer than 2000 shares; $33 plus 3¢ per share for fewer than 2000 shares or more). Display the rival's commission as well as the commission charged by the original broker.

4. Here's a simplified version of the Beaufort scale, which is used to estimate wind force:

|*Speed (knots)*|*Description*|
|-|-|
|Less than 1|Calm|
|1-3|Light air|
|4-27|Breeze|
|28-47|Gale|
|48-63|Storm|
|Above 63|Hurricane|

