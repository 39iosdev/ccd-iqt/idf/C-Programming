# WHILE Loops
While loops utilize a control structure with one component written as an expression.

The component in the head of the while loop is evaluated:

    if TRUE, the loop of the body is executed and the expression is evaluated again
    if FALSE, the program continues with the statment following the loop body

***The EXPRESSION is a requirement.***

#### Note: Take care to avoid infinite loops.

### WHILE Loops execute "loop bodies" multiple times
```c
///while loop syntax///
// expression1 is the condition, evaluated as a boolean expression
while (expression1)		// See above comment
{				// Executed when expression1 != 0
	statement1;		// Regular code
	iteration-statement;	// Ensure you control expression1
}

/* Terminating conditions must be included in the loop body */

/* 
Sequence of for loop execution:
	1.  Is expression1 true?
		2.a.  If so,
			2.a.i   execute the code block
			2.a.iii go back to 1.
		2.b.  If not, stop looping 
*/

///while loop example///
int i = 512;
int counter = 0;

while (i)	// If i != 0…
{		// …execute this
	// Print i
	printf(“i = %d\n”, i);
	// (i /= 2) == (i = i / 2)
	i /= 2;
	counter++; // 1 run counted
	// Is i still != 0?
}		

// Print the number of iterations
printf(“%d runs.\n”, counter);

///while loop example output///
i = 512
i = 256
i = 128
i = 64
i = 32
i = 16
i = 8
i = 4
i = 2
i = 1
//this goes through a total of 10 loops.
```

## COMPLETE PERFORMANCE LAB 16

