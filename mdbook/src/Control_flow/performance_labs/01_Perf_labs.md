5. In one state, single residents are subject to the following income tax:

|*Income*|*Amount of tax*|
|-|-|
|Not over $750|1% of income|
|$750-$2,250|$7.50 plus 2% of ammount over $750|
|$2,250-$3,750|$37.50 plus 3% of ammount over $2,250|
|$3,750-$5,250|$82.50 plus 4% of ammount over $3,750|
|$5,250-$7,000|$142.50 plus 5% of ammount over $7,000|
|Over $7,000|$230.00 plus 6% of ammount over $|

6. Modify the upc.c program that will be supplied to you and check to see whether a UPC is valid. After the user enters a UPC, the program will display either VALID or NOT VALID.

7. Write a program that finds the largest and smallest of four integers entered by the user:

```sh
Enter four integers: 21 43 10 35
Largest: 43
Smallest: 10
```

Use as few ```if``` statements as possible. *Hint*: Four ```if``` statements are sufficient.

8. The following table shows the daily flights from one city to another:

|*Departure time*|*Arrival time*|
|:-:|:-:|
|8:00 a.m. | 10:16 a.m.|
|9:43 a.m. | 11:52 a.m.|
|11:19 a.m.| 1:31 p.m.|
|12:47 p.m.| 3:00 p.m.|
|2:00 p.m.|4:08 p.m.|
|3:45 p.m.|5:55 p.m.|
|7:00 p.m.|9:20 p.m.|
|9:45 p.m.|11:58 p.m.|

Write a program that asks users to enter a time (expressed in hours and minutes, using the 24-hour clock). The program then displays the departure and arrival times for the flight whose departure time is closest to that entered by the user:

```sh
Enter a 24-hour time: 13:15
Closest departure time is 12:47 p.m., arriving at 3:00 p.m.
```
*Hint*: Convert the input into a time expressed in minutes since midnight, and compare it to the departure times, also expressed in minutes since midnight. For example, 13:15 is 13 x 60 + 15 = 795 minutes since midnight, which is closer to 12:47 p.m. (767 minutes since midnight) thank to any of the other departure times.

9. Write a program that prompts the user to enter two dates and then indicates which date comes earlier on the calendar:

```sh
Enter first date (mm/dd/yy): 3/6/08
Enter second date (mm/dd/yy): 5/17/07
5/17/07 is earlier than 3/6/08
```

10. Using the ```switch``` statement, write a program that converts a numerical grade into a letter grade:

```sh
Enter numerical grade: 84
Letter grade: B
```

Use the following grading scale: A = 90-10, B = 80-89, C = 70-79, D = 60-69, F = 0-59. Print an error message if the grade is larger than 100 or less than 0. *Hint*: Break the grade into two digits, then use a ```switch``` statement to test the ten's digit.

11. Write a program that asks the user for a two-digit number, then prints the English word for the number:

```sh
Enter a two-digit number: 45
You entered the number forty-five.
```

*Hint*: Break the number into two digits. Use one ```switch``` statement to print the word for the first digit ("twenty", "thirty", and so forth). Use a second ```switch``` statement to print the word for the second digit. Don't forget that the numbers between 11 and 19 require special treatment.




















