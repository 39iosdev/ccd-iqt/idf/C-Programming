# CONDITONAL COMPILATION

### Using the preprocessor, do the following:
   - Redefine EOF as 66 (without compiler warnings)
   - if true is not defined:
      - declare false as zero
      - declare true as one
      - declare SET_BOOLS as 1
   - else
      - declare SET_BOOLS as 0

inside of main, print SET_BOOLS and EOF.

#### Try this again, this time adding #include<stdbool.h> to the top of the file.