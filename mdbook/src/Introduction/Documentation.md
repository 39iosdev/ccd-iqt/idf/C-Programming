# C Documentation

### Microsoft Developer Network

The [Microsoft Developer Network](https://docs.microsoft.com/en-us/cpp/c-language/?view=vs-2019) docs have extensive, searchable, support for the C Programming Language. To find referance for a function, simply filter by title and type in the function name.

### Linux Man Pages

To find referance material in the linux termial, use the `man` command, followed by the function in question. For example, `man malloc` will bring up information on `malloc()`. 