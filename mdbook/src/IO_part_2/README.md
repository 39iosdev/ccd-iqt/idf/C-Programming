# TOPICS Covered in this section

## File I/O

### The slides for this funxtion are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_2/slides/#/)

# TOPICS Covered in this section; we will re-iterate our Coding style expectations and presentation of Stub Code throughout ch12.

* Coding Style Guide
* Stub Code
* FILE data type
* Related functions

## Requirements
1. Comments
2. Dont Repeat Yourself (DRY)
3. Names
    1. Pointer Variables - append them with _ptr
4. Indent/Brace Style
5. Files
    1. Names - all lowercase, dashes, no underscores, always include initials
6. Headers

## Recommendations
1. Comments
2. Dont Repeat Yourself (DRY)
3. General Formatting
4. Indent/Brace style
5. Variables


**NOTE:** Presentations are not guaranteed to show complete code (AKA: stub code)
