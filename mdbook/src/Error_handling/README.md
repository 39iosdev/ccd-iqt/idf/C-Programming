# OUTLINE

### The slides for this section are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Error_handling/slides/#/)

* General Considerations
* Assert.h
* Errno.h
* Assert vs Errno
* Input/Output Functions 
* Mathematical Functions 
* Pointers 
* Strings 
* Files  

## General Considerations

What should your program do if an ERROR is  encountered?

* retry (more user input)
* ignore (continue processing)
* abort (return/exit)
