# C Programming

**The C Programming MDBook is available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/index.html).**

**The C Programming slides are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/slides/#/).**

The **C Programming Language** is a powerful general-purpose compiled programming language. Compared to Python, C is much faster and allows for access to lower-level functionality, such as direct access to memory.  Despite its low-level capabilities, the language was designed to encourage cross-platform programming. A standards-compliant C program written with portability in mind can be compiled for a wide variety of computer platforms and operating systems with few changes to its source code.  [Wikipedia](https://en.wikipedia.org/wiki/C_(programming_language))

According to the [TIOBE](https://www.tiobe.com/tiobe-index/) index as of 2022, C is the most popular programming language in the world.   It has been the top or second in the index since at least as far back as 1987.

### Chapters
1. [Introduction](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/index.html)
2. [Variables](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Variables/index.html)
3. [Arrays and Strings](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Arrays_strings/index.html)
4. [I/O Part One](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_1/Objectives.html)
5. [Operators and Expressions](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Operators_expressions/index.html)
6. [Bitwise Operations](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/index.html)
7. [Control Flow](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Control_flow/index.html)
8. [Functions](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Functions/index.html)
9. [C Complier](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/C_compiler/index.html)
10. [Preprocessor](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Preprocessor/index.html)
11. [Pointers and Arrays](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Pointers_Arrays/index.html)
12. [I/O Part Two](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_2/index.html)
13. [Memory Management](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Memory_Management/concepts.html)
14. [Scructs](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/index.html)
15. [Error Handling](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Error_handling/index.html)
16. [LinkedLists](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/-/blob/master/mdbook/src/Data_Structures/Linked_list/slides/index.html)
